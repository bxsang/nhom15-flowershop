-- --------------------------------------------------------
-- Host:                         192.168.42.129
-- Server version:               10.3.8-MariaDB - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for flower2
CREATE DATABASE IF NOT EXISTS `flower2` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `flower2`;

-- Dumping structure for table flower2.cart
CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table flower2.cart: ~7 rows (approximately)
DELETE FROM `cart`;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` (`id`, `user_id`, `created_at`, `update_at`) VALUES
	(1, 1, '2021-05-21 14:13:23', '2021-05-21 14:13:23'),
	(2, 1, '2021-05-21 14:13:23', '2021-05-21 14:13:23'),
	(3, 2, '2021-05-21 14:13:23', '2021-05-21 14:13:23'),
	(4, 3, '2021-05-21 14:13:23', '2021-05-21 14:13:23'),
	(5, 4, '2021-05-21 14:13:23', '2021-05-21 14:13:23'),
	(6, 5, '2021-05-21 15:27:42', '2021-05-21 15:27:42'),
	(7, 3, '2021-05-21 15:48:05', '2021-05-21 15:48:05');
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;

-- Dumping structure for table flower2.cart_item
CREATE TABLE IF NOT EXISTS `cart_item` (
  `cart_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`cart_id`,`product_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `cart_item_ibfk_1` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`),
  CONSTRAINT `cart_item_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table flower2.cart_item: ~4 rows (approximately)
DELETE FROM `cart_item`;
/*!40000 ALTER TABLE `cart_item` DISABLE KEYS */;
INSERT INTO `cart_item` (`cart_id`, `product_id`, `quantity`) VALUES
	(1, 1, 2),
	(1, 2, 3),
	(2, 63, 2),
	(4, 63, 6),
	(7, 63, 1);
/*!40000 ALTER TABLE `cart_item` ENABLE KEYS */;

-- Dumping structure for table flower2.category
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table flower2.category: ~4 rows (approximately)
DELETE FROM `category`;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`id`, `name`) VALUES
	(1, 'Sinh nhật'),
	(2, 'Lãng mạn'),
	(3, 'Hoa cưới'),
	(4, 'Sản phẩm khác');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Dumping structure for table flower2.feedback
CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `feedback_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table flower2.feedback: ~0 rows (approximately)
DELETE FROM `feedback`;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` (`id`, `user_id`, `product_id`, `comment`, `created_at`, `update_at`) VALUES
	(1, 1, 1, 'Sản phẩm tốt', '2021-05-21 14:13:24', '2021-05-21 14:13:24'),
	(2, 1, 63, 'Hoa dep', '2021-05-21 15:46:47', '2021-05-21 15:46:47');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;

-- Dumping structure for table flower2.order
CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cart_id` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `phone_number` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_id` (`cart_id`),
  CONSTRAINT `order_ibfk_1` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table flower2.order: ~0 rows (approximately)
DELETE FROM `order`;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` (`id`, `cart_id`, `discount`, `name`, `phone_number`, `address`, `created_at`, `update_at`) VALUES
	(1, 1, 10, 'Admin', '0841234567', 'Quận 7', '2021-05-21 14:13:24', '2021-05-21 14:13:24'),
	(2, 4, 0, 'Nguyen Thi B', '01234567899', 'KTX Đại học Tôn Đức Thắng', '2021-05-21 15:48:05', '2021-05-21 15:48:05');
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

-- Dumping structure for table flower2.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `image_file` varchar(100) DEFAULT NULL,
  `added_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

-- Dumping data for table flower2.product: ~63 rows (approximately)
DELETE FROM `product`;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `name`, `price`, `category_id`, `description`, `quantity`, `discount`, `image_file`, `added_at`, `update_at`, `is_deleted`) VALUES
	(1, 'Crystal Pearl', 499000, 1, 'Bó hoa nhẹ nhàng và thanh khiết với hoa Cẩm Tú Cầu đan xen với những đóa hoa Cúc Tana được gói xinh xắn bằng giấy Kraft. Đây sẽ là món quà xinh xắn và hoàn hảo dành tặng người thương, gia đình hoặc bạn bè.', 15, 0, 'image_flower/ngaysinhnhat/crystalpearl.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(2, 'Combo Reply Me', 699000, 1, 'Combo Reply Me bao gồm: Bó Hoa Best Seller Tana Pure Joy, Bánh Kem Xoài, Thiệp Tình Yêu', 13, 0, 'image_flower/ngaysinhnhat/comboreplyme.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(3, 'First Date', 349000, 1, 'Bó hoa Cúc Tana được gói đặc biệt bằng giấy kraft với dòng chữ "Gorgeous You - For The Beautiful" thể hiện thông điệp một cách tinh tế cho người thương yêu của bạn, phù hợp để bạn gửi tặng vào những dịp đặc biệt.', 12, 0, 'image_flower/ngaysinhnhat/firstdate.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(4, 'Million Little Things', 879000, 1, 'Bó hoa Million Little Things là món quà đáng yêu với Cẩm Tú Cầu vây quanh bởi vô vàn những bông hoa Baby bé li ti nhưng lại rất đỗi xinh xắn, cũng như vô vàn những điều nhỏ bé làm nên chuyện tình cảm tươi đẹp giữa bạn và người nhận vậy đó.', 12, 0, 'image_flower/ngaysinhnhat/millionlittethings.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(5, 'Love At First Sight', 979000, 1, 'Love At First Sight là lời thú nhận ngọt ngào về tình yêu của bạn dành tặng cho người mình luôn thương mến!', 12, 0, 'image_flower/ngaysinhnhat/loveatfirstsight.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(6, 'Timeless Elegance', 379000, 1, 'Bó Hoa Timeless Elegance của nhà Flowerstore mang phong cách Hàn Quốc nhẹ nhàng tươi tắn. Đây chắc chắn sẽ là món quà ngọt ngào và tinh tế dành tặng người thương, gia đình hoặc bạn bè thân yêu!', 12, 0, 'image_flower/ngaysinhnhat/timelesselegance.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(7, 'The Myze', 239000, 1, 'Bó Hoa The Myze của nhà Flowerstore mang phong cách Hàn Quốc nhẹ nhàng tươi tắn. Đây chắc chắn sẽ là món quà ngọt ngào và tinh tế dành tặng người thương, gia đình hoặc bạn bè!', 12, 0, 'image_flower/ngaysinhnhat/themyze.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(8, 'Adorable Smile', 189000, 1, 'Bó Hoa Adorable Smile gồm: 3 Hoa Đồng Tiền, Hoa Bách Nhật', 12, 0, 'image_flower/ngaysinhnhat/adorablesmile.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(9, 'My Princess', 369000, 1, 'Bó hoa ngọt ngào và thanh khiết với hoa Cẩm Tú Cầu, Hoa Cúc và 12 bông hồng da được gói xinh xắn. Đây sẽ là món quà bất ngờ và hoàn hảo dành tặng người thương, gia đình hoặc bạn bè.', 12, 0, 'image_flower/ngaysinhnhat/myprincess.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(10, 'My Blue Heaven', 599000, 1, 'Combo My Blue Heaven bao gồm: Bó hoa Best Sellers Mindful Soul, Hộp Chocolate Ferrero Rocher (5 viên), Gấu Bông Đáng Yêu ', 12, 0, 'image_flower/ngaysinhnhat/myblueheaven.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(11, 'My Way You', 999000, 1, 'Lựa chọn hoàn hảo dành cho người bạn yêu thương với toàn điều ngọt ngào, đáng yêu. Sẵn sàng đốn tim người nhận chưa?', 17, 0, 'image_flower/ngaysinhnhat/mywaytoyou.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(12, 'Wonderland', 1279000, 1, 'Giỏ hoa với sự kết hợp của rất nhiều loại hoa lá, vừa sang trọng, lại vừa xinh xắn, hiện đại, phù hợp gửi tặng cho bất kỳ ai, trong bất kỳ dịp nào.', 12, 0, 'image_flower/ngaysinhnhat/wonderland.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(13, 'Sweet Desire', 369000, 1, 'Sự kết hợp giữa Hoa hồng da, Cẩm tú cầu và hoa Cẩm chướng là một trong lựa chọn hoàn hảo cho ngày đặc biệt hoặc bất kỳ dịp nào để dành tặng cho người mình yêu thương.', 20, 0, 'image_flower/ngaysinhnhat/sweetdesire.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(14, 'Love Me Tender', 399000, 1, 'Bó Hoa Love Me Tender gồm: 3 Hoa Hồng Đỏ, Hoa Baby', 12, 0, 'image_flower/ngaysinhnhat/lovemetender.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(15, 'Combo Be Lovely', 399000, 1, 'Combo Be Lovely bao gồm: Bó Hoa Cẩm Chướng Carla, Thỏ Đan Len, Thiệp Sinh Nhật', 18, 0, 'image_flower/ngaysinhnhat/combobelovely.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(16, 'Burning Love', 639000, 1, 'Bó hoa Burning Love là sự kết hợp của hoa hồng cam nồng nàn, hoa hồng trắng tinh khôi và cả những bông nhím biển tím huyền bí. Như lời khen ngợi của bạn dành cho đối phương, vừa quyến rũ lại vừa xinh đẹp.', 15, 0, 'image_flower/ngaysinhnhat/burninglove.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(17, 'Passionate Love', 599000, 1, 'Bó hoa hồng kết hợp cùng cẩm tú cầu đơn giản được gói rất trẻ trung và thanh lịch là một trong lựa chọn hoàn hảo cho ngày đặc biết hoặc bất kỳ dịp nào để dành tặng cho người mình yêu thương.', 18, 0, 'image_flower/ngaysinhnhat/passionatelove.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(18, 'Intense Love', 679000, 1, 'Bó Hoa Intense Love gồm: 16 bông Hoa Hồng đỏ, Hoa Cẩm Tú Cầu, Các loại hoa & lá khác ', 12, 0, 'image_flower/ngaysinhnhat/intenselove.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(19, 'Love Me Tender', 399000, 1, 'Bó Hoa Love Me Tender gồm: 3 Hoa Hồng Đỏ, Hoa Baby', 12, 0, 'image_flower/ngaysinhnhat/lovemetender.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(20, 'Enchanting Romance', 649000, 1, 'Giỏ hoa tông màu hồng pastel vừa trang nhã vừa ngọt ngào với sự kết hợp của các loại hoa như hoa hồng, hoa đồng tiền, hoa cát tường và hoa cẩm chướng.', 20, 0, 'image_flower/ngaysinhnhat/enchantingromance.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(21, 'Blooms Of Love', 989000, 1, 'Hộp hoa tông màu hồng pastel vừa trang nhã vừa ngọt ngào với sự kết hợp của các loại hoa như hoa hồng, hoa đồng tiền và hoa baby. Giỏ hoa Blooms Of Love thích hợp để tặng cho người thân hay cho bất kỳ ai bạn yêu mến.', 18, 0, 'image_flower/ngaysinhnhat/bloomsoflove.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(22, 'Cupid Kisses', 599000, 1, 'Các loại hoa tông màu đỏ rực rỡ cắm hình trái tim kết hợp với hộp hoa gỗ vintage cổ điển, là món quà sang trọng thích hợp để tặng nhân dịp khai trương hoặc cho bất kỳ dịp đặc biệt.', 18, 0, 'image_flower/ngaysinhnhat/cupidskisses.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(23, 'Coral Pink', 599000, 1, 'Các loại hoa tông màu hồng pastel kết hợp với hộp hoa gỗ vinatge là món quà xinh xắn cho bất kỳ ai bạn yêu mến.', 18, 0, 'image_flower/ngaysinhnhat/coralpink.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(24, 'Combo Red & Gold', 489000, 1, 'Làm ai đó mỉm cười hạnh phúc với bó hoa Fabulously Red đầy rực rỡ và hộp chocolate trứ danh ngọt ngào từ Ferrero Rocher!', 18, 0, 'image_flower/ngaysinhnhat/comboredgold.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(25, 'Enchanted', 1489000, 1, 'Bó hoa Enchanted là khu vườn lộng lẫy sắc thắm đang khoe mình trong ánh nắng chiều xuân có chút lãng mạn, tô điểm bởi hai gam màu pastel và hồng đỏ của những nhành Hồng, Cẩm Chướng, Đồng Tiền, Cẩm Tú Cầu, Phi Yến, và thật nhiều hoa Baby, đủ sức mê hoặc bất kỳ ai.', 18, 0, 'image_flower/ngaysinhnhat/enchanted.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(26, 'Aphrodite', 459000, 2, 'Bó hoa mang gam màu pastel nhẹ nhàng đầy trang nhã điểm xuyến vài bông hồng đỏ thu hút mọi ánh nhìn. Đây sẽ là món quà bất ngờ và hoàn hảo dành tặng người thương, gia đình hoặc bạn bè.', 18, 0, 'image_flower/chude/aphrodite.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(27, 'Summertime', 479000, 2, 'Bó hoa tông màu cam pastel nhẹ nhàng, kết hợp cùng giấy gói màu xanh nhạt vô cùng mát mắt sẽ là món quà bất ngờ và hoàn hảo dành tặng người thương, gia đình hoặc bạn bè.', 18, 0, 'image_flower/chude/summertime.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(28, 'For The Beautiful', 519000, 2, 'Những đóa hoa hồng phấn được gói đặc biệt bằng giấy kraft với dòng chữ Gorgeous You - For The Beautiful thể hiện thông điệp một cách tinh tế cho người thương yêu của bạn, phù hợp để bạn gửi tặng vào những dịp đặc biệt.', 18, 0, 'image_flower/chude/forthebeautiful.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(29, 'Roseanne', 569000, 2, 'Bó Hoa Roseanne gồm: 10 Bông Hoa Hồng Phấn, 3 Bông Hoa Cát Tường, Hoa Baby, Các loại hoa và lá khác', 18, 0, 'image_flower/chude/roseanne.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(30, 'Mandarine Garden', 479000, 2, 'Bó hoa  Mandarine Garden rực rỡ và đầy sức sống với hoa Hồng, Đồng Tiền & Cẩm Chướng tươi tắn, là một phiên bản bó hoa của Spring Garden - sản phẩm bán chạy nhất tại Flowerstore. Đây sẽ là món quà bất ngờ và hoàn hảo dành tặng người thương, gia đình hoặc bạn bè.', 18, 0, 'image_flower/chude/mandarinegarden.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(31, 'Combo Starlight', 599000, 2, 'Bắt đầu tuổi mới bằng toàn sự đáng yêu? Lựa chọn thấu đáo cho người bạn yêu quý!', 18, 0, 'image_flower/chude/combostarlight.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(32, 'Love Me Tender', 399000, 2, 'Bó Hoa Love Me Tender của nhà Flowerstore mang phong cách Hàn Quốc nhẹ nhàng tươi tắn. Đây chắc chắn sẽ là món quà ngọt ngào và tinh tế dành tặng người thương, gia đình hoặc bạn bè thân yêu!', 18, 0, 'image_flower/chude/lovemetender.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(33, 'Combo Shine Sunflower', 1139000, 2, 'Hoa Hướng dương - biểu tượng cho một tình yêu chung thủy kèm theo đó là niềm tin, là hy vọng cho một tình yêu lâu dài và đầy ấm áp. Dù người bạn yêu ở bất cứ đâu, đi bất cứ nơi nào trên thế gian này thì tình yêu của bạn dành cho họ cũng không thay đổi, người bạn yêu giống như mặt trời tỏa sáng và sươi ấm trái tim bạn.', 18, 0, 'image_flower/chude/likeasunflower.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(34, 'Combo Love Rosie', 499000, 2, 'Lựa chọn hoàn hảo dành cho người bạn yêu thương với toàn điều ngọt ngào, lãng mạn. Sẵn sàng đốn tim người nhận chưa?', 25, 0, 'image_flower/chude/comboloveroise.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(35, 'Sweet Desire', 369000, 2, 'Sự kết hợp giữa Hoa hồng da, Cẩm tú cầu và hoa Cẩm chướng là một trong lựa chọn hoàn hảo cho ngày đặc biệt hoặc bất kỳ dịp nào để dành tặng cho người mình yêu thương.', 17, 0, 'image_flower/chude/sweetdesire.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(36, 'Be My Fiancée', 459000, 2, 'Món quà ngọt ngào và tinh tế dành tặng người thương, gia đình hoặc bạn bè.', 18, 0, 'image_flower/chude/myfiancee.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(37, 'Simple Charm', 259000, 2, 'Bó hoa Hồng đỏ đầy lãng mạn kết hợp cùng với giấy gói màu trắng là món quà hoàn hảo thay lời muốn nói gửi đến người thương của bạn vào Valentine hoặc ngày kỷ niệm, sinh nhật.', 18, 0, 'image_flower/chude/simplecharm.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(38, 'My Juliet', 329000, 2, 'Bó hoa pastel được gói theo phong cách Hàn Quốc, mang màu sắc rất trẻ trung và thanh lịch. Sự kết hợp giữa Cẩm tú cầu và hoa Baby là một trong lựa chọn hoàn hảo cho ngày đặc biết hoặc bất kỳ dịp nào để dành tặng cho người mình yêu thương.', 18, 0, 'image_flower/chude/myjuliet.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(39, 'Blooming Lily', 1529000, 2, 'Hộp hoa gỗ với sự kết hợp hài hòa giữa hai tông màu hồng của Lily và màu xanh của lá.', 18, 0, 'image_flower/chude/bloominglily.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(40, 'Sweet Blossom', 769000, 2, 'Hộp hoa gỗ với sự góp mặt của hoa hồng, hoa cẩm tú cầu và cẩm chướng - mang gửi gắm những điều tốt lành đến người nhận.', 18, 0, 'image_flower/chude/sweetblossom.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(41, 'Blooms Of Love', 989000, 2, 'Hộp hoa tông màu hồng pastel vừa trang nhã vừa ngọt ngào với sự kết hợp của các loại hoa như hoa hồng, hoa đồng tiền và hoa baby.', 18, 0, 'image_flower/chude/bloomsoflove.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(42, 'Sweet Tenderness', 799000, 2, 'Hộp hoa gỗ hoành tráng tông màu trang nhã với sự góp mặt của hoa hồng và cẩm tú cầu - những loài hoa mang ý nghĩa vô cùng tốt lành để bạn gửi tặng trong bất kỳ dịp quan trọng nào.', 18, 0, 'image_flower/chude/sweettenderness.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(43, 'Glorious', 1769000, 2, 'Kệ hoa to, tươi tắn và sang trọng với sự kết hợp của các loại hoa màu vàng và tông giấy xanh dịu mắt.', 18, 0, 'image_flower/chude/glorious.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(44, 'Blissful', 2599000, 2, 'Kệ hoa to, tươi tắn và sang trọng với sự kết hợp của nhiều loại hoa. Đây sẽ là món quà tặng đầy ý nghĩa thay cho lời chúc mừng trong dịp khai trương hoặc các ngày lễ trọng đại.', 18, 0, 'image_flower/chude/blissful.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(45, 'Blooming Success', 4759000, 2, 'Kệ hoa to, tươi tắn và sang trọng với sự kết hợp của các loại hoa tông màu hồng. Đây sẽ là món quà tặng đầy ý nghĩa thay cho lời chúc mừng trong dịp khai trương hoặc các ngày lễ trọng đại.', 18, 0, 'image_flower/chude/bloomingsuccess.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(46, 'Chậu Hoa Lan Midnight', 1059000, 2, 'Chậu hoa lan hồ điệp Midnight màu tím đậm sẽ là món quà sang trọng dành tặng cho người bạn yêu quý.', 18, 0, 'image_flower/chude/chauhoalanmidnight.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(47, 'Combo Brilliant', 399000, 2, 'Combo Brilliant dành tặng riêng cho người phụ nữ thông minh, năng động và hiện đại của bạn, thay cho lời cảm ơn sự ra đời của thật nhiều sáng kiến hay, thành tựu vượt bậc mà họ mang lại cho cuộc sống.', 18, 0, 'image_flower/chude/combobrilliant.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(48, 'Combo Best Wishes', 799000, 2, 'Gửi lời chúc mừng ngày sinh nhật tới người bạn yêu quý với bó hoa Cúc Tana, mong người ấy tuổi mới ngát hương và tươi tắn.', 18, 0, 'image_flower/chude/combobestwishes.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(49, 'Love Nest (99 Bông)', 1779000, 2, 'Chiếc tổ uyên ương ngọt ngào lãng mạn là món quà làm ai cũng cảm thấy mình thật đặc biệt trong tim đối phương!', 18, 0, 'image_flower/chude/99bonglovenest.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(50, 'Whole Lot Of Love', 879000, 2, 'Chiếc hộp hình trái tim yêu ngập tràn những bông hoa hồng màu đỏ, đáng yêu như trái tim đang yêu của bạn dành cho người ta vậy đó! Dùng hộp hoa này nói họ biết nha.', 18, 0, 'image_flower/chude/wholelotoflove.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(51, 'Touch Of Spring', 1039000, 2, 'Bó hoa Touch Of Spring xinh đẹp với màu vàng rực rỡ giòn tan như ánh nắng của mỗi buổi chiều xuân, là món quà vừa đáng yêu, vừa mang ý nghĩa sinh sôi tốt lành dành tặng cho người nhận!', 18, 0, 'image_flower/chude/touchofspring.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(52, 'Aphrodite', 459000, 3, 'Bó hoa mang gam màu pastel nhẹ nhàng đầy trang nhã điểm xuyến vài bông hồng đỏ thu hút mọi ánh nhìn. Đây sẽ là món quà bất ngờ và hoàn hảo dành tặng người thương, gia đình hoặc bạn bè.', 18, 0, 'image_flower/loaihoa/aphrodite.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(53, 'Combo Love Me', 799000, 3, 'Bắt đầu tuổi mới bằng toàn sự đáng yêu? Lựa chọn thấu đáo cho người bạn yêu quý!', 18, 0, 'image_flower/loaihoa/comboloveme.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(54, 'Summertime', 479000, 3, 'Bó hoa tông màu cam pastel nhẹ nhàng, kết hợp cùng giấy gói màu xanh nhạt vô cùng mát mắt sẽ là món quà bất ngờ và hoàn hảo dành tặng người thương, gia đình hoặc bạn bè.', 18, 0, 'image_flower/loaihoa/summertime.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(55, 'Cupid Kisses', 599000, 3, 'Các loại hoa tông màu đỏ rực rỡ cắm hình trái tim kết hợp với hộp hoa gỗ vintage cổ điển, là món quà sang trọng thích hợp để tặng nhân dịp khai trương hoặc cho bất kỳ dịp đặc biệt. ', 18, 0, 'image_flower/loaihoa/cupidkisses.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(56, 'Intimate Amber', 869000, 3, 'Những loại hoa màu đỏ kết hợp với giỏ hoa kim loại là món quà cổ điển, sang trọng cho bất kỳ ai bạn yêu mến. ', 18, 0, 'image_flower/loaihoa/intimateamber.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(57, 'Blissful', 2559000, 3, 'Đây sẽ là món quà tặng đầy ý nghĩa thay cho lời chúc mừng trong dịp khai trương hoặc các ngày lễ trọng đại.', 18, 0, 'image_flower/loaihoa/Blissful.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(58, 'Pretty Glory', 759000, 3, 'Hộp hoa gỗ hoành tráng với sự góp mặt của đồng tiền và hướng dương - những loài hoa mang ý nghĩa vô cùng tốt lành để bạn gửi tặng trong bất kỳ dịp quan trọng nào. ', 18, 0, 'image_flower/loaihoa/Pretty.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(59, 'Magnificient', 999000, 3, 'Đây sẽ là món quà tặng đầy ý nghĩa thay cho lời chúc mừng trong dịp khai trương hoặc các ngày lễ trọng đại.', 18, 0, 'image_flower/loaihoa/Magnificient.jpeg', '2021-05-21 14:13:23', '2021-05-21 14:13:23', 0),
	(60, 'Trà Hoa Hồng Rose (20gr)', 159000, 4, 'Hoa hồng là sự lựa chọn hoàn hảo cho mọi bữa tiệc trà. Đây là một loại trà nhẹ và có hương vị nhẹ nhàng sẽ đưa bạn đến với hương thơm ngọt ngào của vườn hoa hồng.', 18, 0, 'image_flower/sanphamkhac/trahoahongrose.jpeg', '2021-05-21 14:13:24', '2021-05-21 14:13:24', 0),
	(61, 'Bộ Quà Tặng Thiên Nhiên', 519000, 4, 'Khăn sợi tre : Thân thiện làn da người lớn và trẻ nhỏ, kháng khuẩn /kháng vi nấm, duy trì độ mềm mại ổn định dù giặt tay hay giặt máy, nhanh khô, thoáng, không ám mùi.', 18, 0, 'image_flower/sanphamkhac/quatangthiennhien.jpeg', '2021-05-21 14:13:24', '2021-05-21 14:13:24', 0),
	(62, 'Hộp Chocolate Ferrero', 149000, 4, 'Ferrero Rocher là dòng sản phẩm chocolate cao cấp đến từ Ý với hương vị đặc biệt, kết hợp hạt phỉ thơm béo, lớp bánh xốp giòn tan và chocolate hảo hạn phủ bên ngoài.', 18, 0, 'image_flower/sanphamkhac/socolaferrero.jpeg', '2021-05-21 14:13:24', '2021-05-21 14:13:24', 0),
	(63, 'Sweet Innocence', 799000, 4, 'Nằm trong Bộ sưu tập Chào đón Giáng Sinh, Hộp hoa Sweet Innocence với sự kết hợp của Dâu tây thanh mát, chocolate nồng nàn cùng chút tinh khôi của Baby trắng và Cúc tana cuối đông sẽ là một món quà ngọt ngào cho người bạn thương vào dịp Noel này đấy.', 12, 0, 'image_flower/sanphamkhac/sweetinnocence.jpeg', '2021-05-21 14:13:24', '2021-05-21 14:13:24', 0),
	(64, 'Test 1', 100000, 1, 'ABCD', 10, 0, 'image_flower/ngaysinhnhat/crystalpearl.jpeg', '2021-05-21 15:50:35', '2021-05-21 15:50:35', 1);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping structure for table flower2.promo_code
CREATE TABLE IF NOT EXISTS `promo_code` (
  `user_id` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `discount` int(11) DEFAULT NULL,
  `maximum_usage` int(11) DEFAULT NULL,
  `usage_count` int(11) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`code`),
  CONSTRAINT `promo_code_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table flower2.promo_code: ~0 rows (approximately)
DELETE FROM `promo_code`;
/*!40000 ALTER TABLE `promo_code` DISABLE KEYS */;
INSERT INTO `promo_code` (`user_id`, `code`, `discount`, `maximum_usage`, `usage_count`, `expiry_date`) VALUES
	(1, 'DC1', 10, 2, 0, '2021-05-26 14:13:22');
/*!40000 ALTER TABLE `promo_code` ENABLE KEYS */;

-- Dumping structure for table flower2.transaction
CREATE TABLE IF NOT EXISTS `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `payment_type` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table flower2.transaction: ~0 rows (approximately)
DELETE FROM `transaction`;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` (`id`, `order_id`, `amount`, `payment_type`, `status`, `created_at`, `update_at`) VALUES
	(1, 1, 2625, 'momo', 0, '2021-05-21 14:13:24', '2021-05-21 14:13:24');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;

-- Dumping structure for table flower2.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(120) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `permissions` int(11) DEFAULT NULL,
  `registered_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table flower2.user: ~4 rows (approximately)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `name`, `email`, `password`, `phone_number`, `address`, `permissions`, `registered_at`, `update_at`) VALUES
	(1, 'Admin', 'admin@example.com', 'pbkdf2:sha256:150000$uO2017at$ae2c1afab9ee3845fb1a0adc2d6e28d3dfecf79101795646417c2ea67e3180f1', '0841234567', 'Quận 7', 1, '2021-05-21 14:13:23', '2021-05-21 14:13:23'),
	(2, 'Admin 2', 'admin2@example.com', 'pbkdf2:sha256:150000$pPqXkap7$ae6dccfbbcd8f91e2314c1ffc6335138b245e6b936124e6294d342a74e029606', '01234567890', 'Quận 7', 1, '2021-05-21 14:13:23', '2021-05-21 14:13:23'),
	(3, 'Nguyễn Văn A', 'nva@example.com', 'pbkdf2:sha256:150000$t34TgtvS$0cfd9b406cecf3840dd0751543dc89a1cdeae6cc1c471def983862cdc986f61c', '01234567899', 'KTX Đại học Tôn Đức Thắng', 0, '2021-05-21 14:13:23', '2021-05-21 14:13:23'),
	(4, 'Nguyễn Thị B', 'ntb@example.com', NULL, '01234567891', 'KTX Đại học Tôn Đức Thắng', 0, '2021-05-21 14:13:23', '2021-05-21 14:13:23'),
	(5, 'Test 1', 'test1@gmail.com', 'pbkdf2:sha256:150000$vJ8j3qLv$0f0a50e9f6bf599a93b0568304c280c0c8e0b89370cdf39cbba811c851418b64', '0123456789', 'Tan Phong', NULL, '2021-05-21 15:27:42', '2021-05-21 15:27:42');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
