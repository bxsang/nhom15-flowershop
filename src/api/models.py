from config import db
from datetime import datetime, timedelta
from werkzeug.security import generate_password_hash, check_password_hash
from flask_jwt_extended import create_access_token
from sqlalchemy import asc, desc
from sqlalchemy.sql.expression import func

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(100))
    phone_number = db.Column(db.String(15))
    address = db.Column(db.String(100))
    permissions = db.Column(db.Integer)
    registered_at = db.Column(db.DateTime, default=datetime.now)
    update_at = db.Column(db.DateTime, default=datetime.now)
    promo_codes = db.relationship('PromoCode', backref=db.backref('user', lazy=True))
    carts = db.relationship('Cart', backref=db.backref('user', lazy=True))
    # orders = db.relationship('Order', backref=db.backref('user', lazy=True))
    # transactions = db.relationship('Transaction', backref=db.backref('user', lazy=True))
    feedbacks = db.relationship('Feedback', backref=db.backref('user', lazy=True))

    def set_password(self, password):
        self.password = generate_password_hash(password)
    
    def check_password(self, password):
        return check_password_hash(self.password, password)

    def generate_jwt(self):
        """
        Generates the Auth Token
        :return: string
        """
        return create_access_token(identity=self)
    
    def get_by_id(self, user_id):
        return self.query.filter_by(id=user_id).one_or_none()

    def get_by_email(self, user_email):
        return self.query.filter_by(email=user_email).one_or_none()

    def create_new(self):
        email_exists = self.get_by_email(self.email) is not None

        if (email_exists):
            return {
                'success': False,
                'msg': 'Email bị trùng, vui lòng thử lại với email khác!'
            }
        else:
            self.set_password(self.password)
            self.carts.append(Cart())
            db.session.add(self)
            try:
                db.session.commit()
                return {
                    'success': True,
                    'msg': 'Đăng ký tài khoản thành công!'
                }
            except Exception as e:
                print('Create user failed: ' + str(e))
                return {
                    'success': False,
                    'msg': 'Đã có lỗi xảy ra, vui lòng thử lại sau!'
                }

    def update(self, data):
        try:
            self.name = data['name']
            if data['current_password'] != '' and data['new_password'] != '':
                if not self.check_password(data['current_password']):
                    return {
                        'success': False,
                        'msg': 'Mật khẩu cũ không chính xác'
                    }
                else:
                    self.set_password(data['new_password'])
            self.phone_number = int(data['phone_number'])
            self.address = data['address']
            self.update_at = datetime.now()
            db.session.commit()
            return {
                'success': True,
                'msg': 'Cập nhật thông tài khoản thành công'
            }
        except Exception as e:
            print('Update user failed: ' + str(e))
            return {
                'success': False,
                'msg': 'Có lỗi xảy ra, vui lòng thử lại sau!'
            }

class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    products = db.relationship('Product', backref=db.backref('category', lazy=True))

class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    price = db.Column(db.Integer)
    category_id = db.Column(db.Integer, db.ForeignKey("category.id"))
    description = db.Column(db.Text)
    quantity = db.Column(db.Integer)
    discount = db.Column(db.Integer)
    image_file = db.Column(db.String(100))
    added_at = db.Column(db.DateTime, default=datetime.now)
    update_at = db.Column(db.DateTime, default=datetime.now)
    is_deleted = db.Column(db.Boolean, default=False)
    carts = db.relationship('CartItem', uselist=False, backref=db.backref('product', lazy=True))
    feedbacks = db.relationship('Feedback', backref=db.backref('product', lazy=True))

    def get_by_id(self, product_id):
        return self.query.filter_by(id=product_id).one_or_none()
    
    def get_random(self, offset, limit):
        return self.query.order_by(func.random()).offset(offset).limit(limit).all()
    
    def get_newest(self, offset, limit):
        return self.query.order_by(desc(Product.id)).offset(offset).limit(limit).all()
    
    def get_oldest(self, offset, limit):
        return self.query.order_by(asc(Product.id)).offset(offset).limit(limit).all()
    
    def get_alphabetical(self, offset, limit):
        return self.query.order_by(Product.name).offset(offset).limit(limit).all()

    def get_most_ordered(self, offset, limit):
        products = []
        orders = Order.query.join(Cart.query.join(CartItem.query.join(Product))).order_by(func.count()).offset(offset).limit(limit).all()
        for order in orders:
            cart_items = order.cart.items
            for cart_item in cart_items:
                product = cart_item.product
                if product in products:
                    continue
                products.append(product)
        return products
    
    def search(self, text, offset, limit):
        return self.query.filter(Product.name.like('%' + text + '%')).order_by(Product.name).all()

    def create_new(self):
        category = Category.query.filter_by(id=self.category_id).one_or_none()
        if category is not None:
            try:
                category.products.append(self)
                db.session.add(category)
                db.session.commit()
                return {
                    'success': True,
                    'id': self.id
                }
            except Exception as e:
                print('Create product failed: ' + str(e))
                return {
                    'success': False,
                    'msg': 'Có lỗi xảy ra, vui lòng thử lại sau!'
                }
        return {
            'success': False,
            'msg': 'Mã loại không tồn tại'
        }

    def update(self, data):
        try:
            self.name = data['name']
            self.price = data['price']
            self.description = data['description']
            self.quantity = data['quantity']
            self.discount = data['discount']
            self.image_file = data['image_file']
            self.category_id = data['category_id']
            db.session.commit()
            return {
                'success': True,
                'msg': 'Cập nhật thông tin sản phẩm thành công'
            }
        except Exception as e:
            print('Update product failed: ' + str(e))
            return {
                'success': False,
                'msg': 'Có lỗi xảy ra, vui lòng thử lại sau!'
            }

    def delete(self):
        try:
            self.is_deleted = True
            db.session.commit()
            return {
                'success': True,
                'msg': 'Xoá sản phẩm thành công'
            }
        except Exception as e:
            print('Delete product failed: ' + str(e))
            return {
                'success': False,
                'msg': 'Có lỗi xảy ra, vui lòng thử lại sau!'
            }

    def add_feedback(self, user_id, comment):
        feedback = Feedback(user_id=user_id, comment=comment)
        try:
            self.feedbacks.append(feedback)
            db.session.commit()
            return {
                'success': True,
                'msg': 'Thêm feedback thành công'
            }
        except Exception as e:
            print('Add feedback failed: ' + str(e))
            return {
                'success': False,
                'msg': 'Có lỗi xảy ra, vui lòng thử lại sau!'
            }

class PromoCode(db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), primary_key=True)
    code = db.Column(db.String(20), primary_key=True)
    discount = db.Column(db.Integer)
    maximum_usage = db.Column(db.Integer)
    usage_count = db.Column(db.Integer, default=0)
    expiry_date = db.Column(db.DateTime, default=datetime.now() + timedelta(days=5))

class CartItem(db.Model):
    cart_id = db.Column(db.Integer, db.ForeignKey("cart.id"), primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey("product.id"), primary_key=True)
    quantity = db.Column(db.Integer)

class Cart(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    created_at = db.Column(db.DateTime, default=datetime.now)
    update_at = db.Column(db.DateTime, default=datetime.now)
    items = db.relationship('CartItem', backref=db.backref('cart', lazy=True))
    order = db.relationship('Order', uselist=False, backref=db.backref('cart', lazy=True))

    def get_by_id(self, cart_id):
        return self.query.filter_by(id=cart_id).one_or_none()

    def get_one_by_user_id(self, user_id):
        return self.query.filter_by(user_id=user_id).order_by(desc(Cart.id)).limit(1).one_or_none()
    
    def get_by_user_id(self, user_id, offset, limit):
        return self.query.filter_by(user_id=user_id).order_by(desc(Cart.id)).offset(offset).limit(limit).all()
    
    def add_product(self, product_id, quantity):
        cart_item = CartItem.query.filter_by(cart_id=self.id, product_id=product_id).one_or_none()
        if cart_item is not None:
            cart_item.quantity += quantity
            try:
                db.session.add(cart_item)
                db.session.commit()
                return {
                    'success': True,
                    'id': self.id
                }
            except Exception as e:
                print('Increase product quantity to cart failed: ' + str(e))
                return {
                    'success': False,
                    'msg': 'Có lỗi xảy ra, vui lòng thử lại sau!'
                }
        else:
            try:
                cart_item = CartItem(product_id=product_id, quantity=quantity)
                cart_item.cart = self
                db.session.add(cart_item)
                db.session.commit()
                return {
                    'success': True,
                    'id': cart_item.cart_id
                }
            except Exception as e:
                print('Add product to cart failed: ' + str(e))
                return {
                    'success': False,
                    'msg': 'Có lỗi xảy ra, vui lòng thử lại sau!'
                }

    def update_item_quantity(self, product_id, quantity):
        try:
            cart_item = CartItem.query.filter_by(cart_id=self.id, product_id=product_id).one_or_none()
            cart_item.quantity = quantity
            db.session.add(cart_item)
            db.session.commit()
            return {
                'success': True,
                'id': cart_item.cart_id
            }
        except Exception as e:
            print('Update product quantity failed: ' + str(e))
            return {
                'success': False,
                'msg': 'Có lỗi xảy ra, vui lòng thử lại sau!'
            }

    def orderCart(self, user, name, phone_number, address):
        if self.order is None:
            try:
                order = Order(discount=0, name=name, phone_number=phone_number, address=address)
                self.order = order
                for item in self.items:
                    product = Product().get_by_id(item.product_id)
                    product.quantity -= item.quantity
                user.carts.append(Cart())
                db.session.commit()
                return {
                    'success': True,
                    'msg': 'Thêm order thành công'
                }
            except Exception as e:
                print('Add order failed: ' + str(e))
                return {
                    'success': False,
                    'msg': 'Có lỗi xảy ra, vui lòng thử lại sau!'
                }
        else:
           return {
                'success': False,
                'msg': 'Mã order bị trùng'
            }

    def delete_one_product(self, product_id):
        try:
            cart_item = CartItem.query.filter_by(cart_id=self.id, product_id=product_id).one_or_none()
            db.session.delete(cart_item)
            db.session.commit()
            return {
                'success': True,
                'msg': 'Xoá sản phẩm khỏi giỏ hàng thành công'
            }
        except Exception as e:
            print('Delete product from cart failed: ' + str(e))
            return {
                'success': False,
                'msg': 'Có lỗi xảy ra, vui lòng thử lại sau!'
            }

class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    cart_id = db.Column(db.Integer, db.ForeignKey("cart.id"))
    discount = db.Column(db.Integer)
    name = db.Column(db.String(100))
    phone_number = db.Column(db.String(100))
    address = db.Column(db.String(100))
    created_at = db.Column(db.DateTime, default=datetime.now)
    update_at = db.Column(db.DateTime, default=datetime.now)
    transaction = db.relationship('Transaction', uselist=False, backref=db.backref('order', lazy=True))

    def get_by_id(self, order_id):
        return self.query.filter_by(id=order_id).one_or_none()

    def get_by_user_id(self, user_id, offset, limit):
        result = []
        carts = Cart().get_by_user_id(user_id, offset, limit)
        for cart in carts:
            products = []
            grand_total = 0
            for item in cart.items:
                product = Product().get_by_id(item.product_id)
                products.append({
                    'product_id': item.product_id,
                    'product_name': product.name,
                    'product_price': product.price,
                    'category': product.category.name,
                    'description': product.description,
                    'quantity': product.quantity,
                    'discount': product.discount,
                    'image_file': product.image_file,
                    'added_at': str(product.added_at),
                    'update_at': str(product.update_at),
                    'quantity': item.quantity
                })
                grand_total += int(product.price)*int(item.quantity)
            if cart.order is not None:
                result.append({
                    'id': cart.order.id,
                    'cart_id': cart.order.cart_id,
                    'discount': cart.order.discount,
                    'name': cart.order.name,
                    'phone_number': cart.order.phone_number,
                    'address': cart.order.address,
                    'created_at': str(cart.order.created_at),
                    'update_at': str(cart.order.update_at),
                    'products': products,
                    'grand_total': grand_total
                })
        return result

    def get_by_all_users(self, offset, limit):
        result = []
        carts  = Cart.query.order_by(desc(Cart.id)).offset(offset).limit(limit).all()
        for cart in carts:
            products = []
            grand_total = 0
            for item in cart.items:
                product = Product().get_by_id(item.product_id)
                products.append({
                    'product_id': item.product_id,
                    'product_name': product.name,
                    'product_price': product.price,
                    'category': product.category.name,
                    'description': product.description,
                    'quantity': product.quantity,
                    'discount': product.discount,
                    'image_file': product.image_file,
                    'added_at': str(product.added_at),
                    'update_at': str(product.update_at),
                    'quantity': item.quantity
                })
                grand_total += int(product.price)*int(item.quantity)
            if cart.order is not None:
                result.append({
                    'id': cart.order.id,
                    'cart_id': cart.order.cart_id,
                    'discount': cart.order.discount,
                    'name': cart.order.name,
                    'phone_number': cart.order.phone_number,
                    'address': cart.order.address,
                    'created_at': str(cart.order.created_at),
                    'update_at': str(cart.order.update_at),
                    'products': products,
                    'grand_total': grand_total
                })
        return result

class Transaction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    order_id = db.Column(db.Integer, db.ForeignKey("order.id"))
    amount = db.Column(db.Integer)
    payment_type = db.Column(db.String(100))
    status = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime, default=datetime.now)
    update_at = db.Column(db.DateTime, default=datetime.now)

class Feedback(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    product_id = db.Column(db.Integer, db.ForeignKey("product.id"))
    comment = db.Column(db.Text)
    created_at = db.Column(db.DateTime, default=datetime.now)
    update_at = db.Column(db.DateTime, default=datetime.now)

    def get_by_id(self, feedback_id):
        return self.query.filter_by(id=feedback_id).one_or_none()
