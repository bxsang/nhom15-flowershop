from config import db
import models

db.create_all()

user = models.User(name='Admin', email='admin@example.com', phone_number='0841234567', address='Quận 7', permissions=1)
user2 = models.User(name='Admin 2', email='admin2@example.com', phone_number='01234567890', address='Quận 7', permissions=1)
user3 = models.User(name='Nguyễn Văn A', email='nva@example.com', phone_number='01234567899', address='KTX Đại học Tôn Đức Thắng', permissions=0)
user4 = models.User(name='Nguyễn Thị B', email='ntb@example.com', phone_number='01234567891', address='KTX Đại học Tôn Đức Thắng', permissions=0)

user.set_password('1234')
user2.set_password('1234')
user3.set_password('1234')
user3.set_password('1234')

category = models.Category(name='Sinh nhật')
category2 = models.Category(name='Lãng mạn')
category3 = models.Category(name='Hoa cưới')
category4 = models.Category(name='Sản phẩm khác')

# Ngày sinh nhật
product = models.Product(
  name='Crystal Pearl', 
  price=499000,
  description='Bó hoa nhẹ nhàng và thanh khiết với hoa Cẩm Tú Cầu đan xen với những đóa hoa Cúc Tana được gói xinh xắn bằng giấy Kraft. Đây sẽ là món quà xinh xắn và hoàn hảo dành tặng người thương, gia đình hoặc bạn bè.', 
  quantity=15, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/crystalpearl.jpeg'
  )

product2 = models.Product(
  name='Combo Reply Me', 
  price=699000,
  description='Combo Reply Me bao gồm: Bó Hoa Best Seller Tana Pure Joy, Bánh Kem Xoài, Thiệp Tình Yêu', 
  quantity=13, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/comboreplyme.jpeg'
  )

product3 = models.Product(
  name='First Date', 
  price=349000,
  description='Bó hoa Cúc Tana được gói đặc biệt bằng giấy kraft với dòng chữ "Gorgeous You - For The Beautiful" thể hiện thông điệp một cách tinh tế cho người thương yêu của bạn, phù hợp để bạn gửi tặng vào những dịp đặc biệt.', 
  quantity=12, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/firstdate.jpeg'
  )

product4 = models.Product(
  name='Million Little Things', 
  price=879000,
  description='Bó hoa Million Little Things là món quà đáng yêu với Cẩm Tú Cầu vây quanh bởi vô vàn những bông hoa Baby bé li ti nhưng lại rất đỗi xinh xắn, cũng như vô vàn những điều nhỏ bé làm nên chuyện tình cảm tươi đẹp giữa bạn và người nhận vậy đó.', 
  quantity=12, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/millionlittethings.jpeg'
  )

product5 = models.Product(
  name='Love At First Sight', 
  price=979000,
  description='Love At First Sight là lời thú nhận ngọt ngào về tình yêu của bạn dành tặng cho người mình luôn thương mến!', 
  quantity=12, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/loveatfirstsight.jpeg'
  )

product6 = models.Product(
  name='Timeless Elegance', 
  price=379000,
  description='Bó Hoa Timeless Elegance của nhà Flowerstore mang phong cách Hàn Quốc nhẹ nhàng tươi tắn. Đây chắc chắn sẽ là món quà ngọt ngào và tinh tế dành tặng người thương, gia đình hoặc bạn bè thân yêu!', 
  quantity=12, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/timelesselegance.jpeg')

product7 = models.Product(
  name='The Myze', 
  price=239000,
  description='Bó Hoa The Myze của nhà Flowerstore mang phong cách Hàn Quốc nhẹ nhàng tươi tắn. Đây chắc chắn sẽ là món quà ngọt ngào và tinh tế dành tặng người thương, gia đình hoặc bạn bè!', 
  quantity=12, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/themyze.jpeg'
  )

product8 = models.Product(
  name='Adorable Smile', 
  price=189000,
  description='Bó Hoa Adorable Smile gồm: 3 Hoa Đồng Tiền, Hoa Bách Nhật', 
  quantity=12, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/adorablesmile.jpeg'
  )

product9 = models.Product(
  name='My Princess', 
  price=369000,
  description='Bó hoa ngọt ngào và thanh khiết với hoa Cẩm Tú Cầu, Hoa Cúc và 12 bông hồng da được gói xinh xắn. Đây sẽ là món quà bất ngờ và hoàn hảo dành tặng người thương, gia đình hoặc bạn bè.', 
  quantity=12, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/myprincess.jpeg'
  )

product10 = models.Product(
  name='My Blue Heaven', 
  price=599000,
  description='Combo My Blue Heaven bao gồm: Bó hoa Best Sellers Mindful Soul, Hộp Chocolate Ferrero Rocher (5 viên), Gấu Bông Đáng Yêu ', 
  quantity=12, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/myblueheaven.jpeg'
  )

product11 = models.Product(
  name='My Way You', 
  price=999000,
  description='Lựa chọn hoàn hảo dành cho người bạn yêu thương với toàn điều ngọt ngào, đáng yêu. Sẵn sàng đốn tim người nhận chưa?', 
  quantity=17, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/mywaytoyou.jpeg'
  )

product12 = models.Product(
  name='Wonderland', 
  price=1279000,
  description='Giỏ hoa với sự kết hợp của rất nhiều loại hoa lá, vừa sang trọng, lại vừa xinh xắn, hiện đại, phù hợp gửi tặng cho bất kỳ ai, trong bất kỳ dịp nào.', 
  quantity=12, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/wonderland.jpeg'
  )

product13 = models.Product(
  name='Sweet Desire', 
  price=369000,
  description='Sự kết hợp giữa Hoa hồng da, Cẩm tú cầu và hoa Cẩm chướng là một trong lựa chọn hoàn hảo cho ngày đặc biệt hoặc bất kỳ dịp nào để dành tặng cho người mình yêu thương.', 
  quantity=20, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/sweetdesire.jpeg'
  )

product14 = models.Product(
  name='Love Me Tender', 
  price=399000,
  description='Bó Hoa Love Me Tender gồm: 3 Hoa Hồng Đỏ, Hoa Baby', 
  quantity=12, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/lovemetender.jpeg'
  )

product15 = models.Product(
  name='Combo Be Lovely', 
  price=399000,
  description='Combo Be Lovely bao gồm: Bó Hoa Cẩm Chướng Carla, Thỏ Đan Len, Thiệp Sinh Nhật', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/combobelovely.jpeg'
  )

product16 = models.Product(
  name='Burning Love', 
  price=639000,
  description='Bó hoa Burning Love là sự kết hợp của hoa hồng cam nồng nàn, hoa hồng trắng tinh khôi và cả những bông nhím biển tím huyền bí. Như lời khen ngợi của bạn dành cho đối phương, vừa quyến rũ lại vừa xinh đẹp.', 
  quantity=15, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/burninglove.jpeg'
  )

product17 = models.Product(
  name='Passionate Love', 
  price=599000,
  description='Bó hoa hồng kết hợp cùng cẩm tú cầu đơn giản được gói rất trẻ trung và thanh lịch là một trong lựa chọn hoàn hảo cho ngày đặc biết hoặc bất kỳ dịp nào để dành tặng cho người mình yêu thương.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/passionatelove.jpeg'
  )

product18 = models.Product(
  name='Intense Love', 
  price=679000,
  description='Bó Hoa Intense Love gồm: 16 bông Hoa Hồng đỏ, Hoa Cẩm Tú Cầu, Các loại hoa & lá khác ', 
  quantity=12, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/intenselove.jpeg'
  )

product19 = models.Product(
  name='Love Me Tender', 
  price=399000,
  description='Bó Hoa Love Me Tender gồm: 3 Hoa Hồng Đỏ, Hoa Baby', 
  quantity=12, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/lovemetender.jpeg'
  )

product20 = models.Product(
  name='Enchanting Romance', 
  price=649000,
  description='Giỏ hoa tông màu hồng pastel vừa trang nhã vừa ngọt ngào với sự kết hợp của các loại hoa như hoa hồng, hoa đồng tiền, hoa cát tường và hoa cẩm chướng.', 
  quantity=20, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/enchantingromance.jpeg'
  )

product21 = models.Product(
  name='Blooms Of Love', 
  price=989000,
  description='Hộp hoa tông màu hồng pastel vừa trang nhã vừa ngọt ngào với sự kết hợp của các loại hoa như hoa hồng, hoa đồng tiền và hoa baby. Giỏ hoa Blooms Of Love thích hợp để tặng cho người thân hay cho bất kỳ ai bạn yêu mến.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/bloomsoflove.jpeg'
  )

product22 = models.Product(
  name='Cupid Kisses', 
  price=599000,
  description='Các loại hoa tông màu đỏ rực rỡ cắm hình trái tim kết hợp với hộp hoa gỗ vintage cổ điển, là món quà sang trọng thích hợp để tặng nhân dịp khai trương hoặc cho bất kỳ dịp đặc biệt.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/cupidskisses.jpeg'
  )

product23 = models.Product(
  name='Coral Pink', 
  price=599000,
  description='Các loại hoa tông màu hồng pastel kết hợp với hộp hoa gỗ vinatge là món quà xinh xắn cho bất kỳ ai bạn yêu mến.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/coralpink.jpeg'
  )

product24 = models.Product(
  name='Combo Red & Gold', 
  price=489000,
  description='Làm ai đó mỉm cười hạnh phúc với bó hoa Fabulously Red đầy rực rỡ và hộp chocolate trứ danh ngọt ngào từ Ferrero Rocher!', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/comboredgold.jpeg'
  )

product25 = models.Product(
  name='Enchanted', 
  price=1489000,
  description='Bó hoa Enchanted là khu vườn lộng lẫy sắc thắm đang khoe mình trong ánh nắng chiều xuân có chút lãng mạn, tô điểm bởi hai gam màu pastel và hồng đỏ của những nhành Hồng, Cẩm Chướng, Đồng Tiền, Cẩm Tú Cầu, Phi Yến, và thật nhiều hoa Baby, đủ sức mê hoặc bất kỳ ai.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/ngaysinhnhat/enchanted.jpeg'
  )

category.products.append(product)
category.products.append(product2)
category.products.append(product3)
category.products.append(product4)
category.products.append(product5)
category.products.append(product6)
category.products.append(product7)
category.products.append(product8)
category.products.append(product9)
category.products.append(product10)
category.products.append(product11)
category.products.append(product12)
category.products.append(product13)
category.products.append(product14)
category.products.append(product15)
category.products.append(product16)
category.products.append(product17)
category.products.append(product18)
category.products.append(product19)
category.products.append(product20)
category.products.append(product21)
category.products.append(product22)
category.products.append(product23)
category.products.append(product24)
category.products.append(product25)

#Chủ đề
product26 = models.Product(
  name='Aphrodite', 
  price=459000,
  description='Bó hoa mang gam màu pastel nhẹ nhàng đầy trang nhã điểm xuyến vài bông hồng đỏ thu hút mọi ánh nhìn. Đây sẽ là món quà bất ngờ và hoàn hảo dành tặng người thương, gia đình hoặc bạn bè.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/aphrodite.jpeg'
  )

product27 = models.Product(
  name='Summertime', 
  price=479000,
  description='Bó hoa tông màu cam pastel nhẹ nhàng, kết hợp cùng giấy gói màu xanh nhạt vô cùng mát mắt sẽ là món quà bất ngờ và hoàn hảo dành tặng người thương, gia đình hoặc bạn bè.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/summertime.jpeg'
  )

product28 = models.Product(
  name='For The Beautiful', 
  price=519000,
  description='Những đóa hoa hồng phấn được gói đặc biệt bằng giấy kraft với dòng chữ Gorgeous You - For The Beautiful thể hiện thông điệp một cách tinh tế cho người thương yêu của bạn, phù hợp để bạn gửi tặng vào những dịp đặc biệt.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/forthebeautiful.jpeg'
  )

product29 = models.Product(
  name='Roseanne', 
  price=569000,
  description='Bó Hoa Roseanne gồm: 10 Bông Hoa Hồng Phấn, 3 Bông Hoa Cát Tường, Hoa Baby, Các loại hoa và lá khác', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/roseanne.jpeg'
  )

product30 = models.Product(
  name='Mandarine Garden', 
  price=479000,
  description='Bó hoa  Mandarine Garden rực rỡ và đầy sức sống với hoa Hồng, Đồng Tiền & Cẩm Chướng tươi tắn, là một phiên bản bó hoa của Spring Garden - sản phẩm bán chạy nhất tại Flowerstore. Đây sẽ là món quà bất ngờ và hoàn hảo dành tặng người thương, gia đình hoặc bạn bè.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/mandarinegarden.jpeg'
  )

product31 = models.Product(
  name='Combo Starlight', 
  price=599000,
  description='Bắt đầu tuổi mới bằng toàn sự đáng yêu? Lựa chọn thấu đáo cho người bạn yêu quý!', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/combostarlight.jpeg'
  )

product32 = models.Product(
  name='Love Me Tender', 
  price=399000,
  description='Bó Hoa Love Me Tender của nhà Flowerstore mang phong cách Hàn Quốc nhẹ nhàng tươi tắn. Đây chắc chắn sẽ là món quà ngọt ngào và tinh tế dành tặng người thương, gia đình hoặc bạn bè thân yêu!', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/lovemetender.jpeg'
  )

product33 = models.Product(
  name='Combo Shine Sunflower', 
  price=1139000,
  description='Hoa Hướng dương - biểu tượng cho một tình yêu chung thủy kèm theo đó là niềm tin, là hy vọng cho một tình yêu lâu dài và đầy ấm áp. Dù người bạn yêu ở bất cứ đâu, đi bất cứ nơi nào trên thế gian này thì tình yêu của bạn dành cho họ cũng không thay đổi, người bạn yêu giống như mặt trời tỏa sáng và sươi ấm trái tim bạn.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/likeasunflower.jpeg'
  )

product34 = models.Product(
  name='Combo Love Rosie', 
  price=499000,
  description='Lựa chọn hoàn hảo dành cho người bạn yêu thương với toàn điều ngọt ngào, lãng mạn. Sẵn sàng đốn tim người nhận chưa?', 
  quantity=25, 
  discount=0, 
  image_file='image_flower/chude/comboloveroise.jpeg'
  )

product35 = models.Product(
  name='Sweet Desire', 
  price=369000,
  description='Sự kết hợp giữa Hoa hồng da, Cẩm tú cầu và hoa Cẩm chướng là một trong lựa chọn hoàn hảo cho ngày đặc biệt hoặc bất kỳ dịp nào để dành tặng cho người mình yêu thương.', 
  quantity=17, 
  discount=0, 
  image_file='image_flower/chude/sweetdesire.jpeg'
  )

product36 = models.Product(
  name='Be My Fiancée', 
  price=459000,
  description='Món quà ngọt ngào và tinh tế dành tặng người thương, gia đình hoặc bạn bè.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/myfiancee.jpeg'
  )

product37 = models.Product(
  name='Simple Charm', 
  price=259000,
  description='Bó hoa Hồng đỏ đầy lãng mạn kết hợp cùng với giấy gói màu trắng là món quà hoàn hảo thay lời muốn nói gửi đến người thương của bạn vào Valentine hoặc ngày kỷ niệm, sinh nhật.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/simplecharm.jpeg'
  )

product38 = models.Product(
  name='My Juliet', 
  price=329000,
  description='Bó hoa pastel được gói theo phong cách Hàn Quốc, mang màu sắc rất trẻ trung và thanh lịch. Sự kết hợp giữa Cẩm tú cầu và hoa Baby là một trong lựa chọn hoàn hảo cho ngày đặc biết hoặc bất kỳ dịp nào để dành tặng cho người mình yêu thương.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/myjuliet.jpeg'
  )

product39 = models.Product(
  name='Blooming Lily', 
  price=1529000,
  description='Hộp hoa gỗ với sự kết hợp hài hòa giữa hai tông màu hồng của Lily và màu xanh của lá.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/bloominglily.jpeg'
  )

product40 = models.Product(
  name='Sweet Blossom', 
  price=769000,
  description='Hộp hoa gỗ với sự góp mặt của hoa hồng, hoa cẩm tú cầu và cẩm chướng - mang gửi gắm những điều tốt lành đến người nhận.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/sweetblossom.jpeg'
  )

product41 = models.Product(
  name='Blooms Of Love', 
  price=989000,
  description='Hộp hoa tông màu hồng pastel vừa trang nhã vừa ngọt ngào với sự kết hợp của các loại hoa như hoa hồng, hoa đồng tiền và hoa baby.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/bloomsoflove.jpeg'
  )

product42 = models.Product(
  name='Sweet Tenderness', 
  price=799000,
  description='Hộp hoa gỗ hoành tráng tông màu trang nhã với sự góp mặt của hoa hồng và cẩm tú cầu - những loài hoa mang ý nghĩa vô cùng tốt lành để bạn gửi tặng trong bất kỳ dịp quan trọng nào.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/sweettenderness.jpeg'
  )

product43 = models.Product(
  name='Glorious', 
  price=1769000,
  description='Kệ hoa to, tươi tắn và sang trọng với sự kết hợp của các loại hoa màu vàng và tông giấy xanh dịu mắt.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/glorious.jpeg'
  )

product44 = models.Product(
  name='Blissful', 
  price=2599000,
  description='Kệ hoa to, tươi tắn và sang trọng với sự kết hợp của nhiều loại hoa. Đây sẽ là món quà tặng đầy ý nghĩa thay cho lời chúc mừng trong dịp khai trương hoặc các ngày lễ trọng đại.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/blissful.jpeg'
  )

product45 = models.Product(
  name='Blooming Success', 
  price=4759000,
  description='Kệ hoa to, tươi tắn và sang trọng với sự kết hợp của các loại hoa tông màu hồng. Đây sẽ là món quà tặng đầy ý nghĩa thay cho lời chúc mừng trong dịp khai trương hoặc các ngày lễ trọng đại.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/bloomingsuccess.jpeg'
  )

product46 = models.Product(
  name='Chậu Hoa Lan Midnight', 
  price=1059000,
  description='Chậu hoa lan hồ điệp Midnight màu tím đậm sẽ là món quà sang trọng dành tặng cho người bạn yêu quý.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/chauhoalanmidnight.jpeg'
  )

product47 = models.Product(
  name='Combo Brilliant', 
  price=399000,
  description='Combo Brilliant dành tặng riêng cho người phụ nữ thông minh, năng động và hiện đại của bạn, thay cho lời cảm ơn sự ra đời của thật nhiều sáng kiến hay, thành tựu vượt bậc mà họ mang lại cho cuộc sống.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/combobrilliant.jpeg'
  )

product48 = models.Product(
  name='Combo Best Wishes', 
  price=799000,
  description='Gửi lời chúc mừng ngày sinh nhật tới người bạn yêu quý với bó hoa Cúc Tana, mong người ấy tuổi mới ngát hương và tươi tắn.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/combobestwishes.jpeg'
  )

product49 = models.Product(
  name='Love Nest (99 Bông)', 
  price=1779000,
  description='Chiếc tổ uyên ương ngọt ngào lãng mạn là món quà làm ai cũng cảm thấy mình thật đặc biệt trong tim đối phương!', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/99bonglovenest.jpeg'
  )

product50 = models.Product(
  name='Whole Lot Of Love', 
  price=879000,
  description='Chiếc hộp hình trái tim yêu ngập tràn những bông hoa hồng màu đỏ, đáng yêu như trái tim đang yêu của bạn dành cho người ta vậy đó! Dùng hộp hoa này nói họ biết nha.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/wholelotoflove.jpeg'
  )

product51 = models.Product(
  name='Touch Of Spring', 
  price=1039000,
  description='Bó hoa Touch Of Spring xinh đẹp với màu vàng rực rỡ giòn tan như ánh nắng của mỗi buổi chiều xuân, là món quà vừa đáng yêu, vừa mang ý nghĩa sinh sôi tốt lành dành tặng cho người nhận!', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/chude/touchofspring.jpeg'
  )

category2.products.append(product26)
category2.products.append(product27)
category2.products.append(product28)
category2.products.append(product29)
category2.products.append(product30)
category2.products.append(product31)
category2.products.append(product32)
category2.products.append(product33)
category2.products.append(product34)
category2.products.append(product35)
category2.products.append(product36)
category2.products.append(product37)
category2.products.append(product38)
category2.products.append(product39)
category2.products.append(product40)
category2.products.append(product41)
category2.products.append(product42)
category2.products.append(product43)
category2.products.append(product44)
category2.products.append(product45)
category2.products.append(product46)
category2.products.append(product47)
category2.products.append(product48)
category2.products.append(product49)
category2.products.append(product50)
category2.products.append(product51)

#Loại hoa
product52 = models.Product(
  name='Aphrodite', 
  price=459000,
  description='Bó hoa mang gam màu pastel nhẹ nhàng đầy trang nhã điểm xuyến vài bông hồng đỏ thu hút mọi ánh nhìn. Đây sẽ là món quà bất ngờ và hoàn hảo dành tặng người thương, gia đình hoặc bạn bè.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/loaihoa/aphrodite.jpeg'
  )

product53 = models.Product(
  name='Combo Love Me', 
  price=799000,
  description='Bắt đầu tuổi mới bằng toàn sự đáng yêu? Lựa chọn thấu đáo cho người bạn yêu quý!', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/loaihoa/comboloveme.jpeg'
  )

product54 = models.Product(
  name='Summertime', 
  price=479000,
  description='Bó hoa tông màu cam pastel nhẹ nhàng, kết hợp cùng giấy gói màu xanh nhạt vô cùng mát mắt sẽ là món quà bất ngờ và hoàn hảo dành tặng người thương, gia đình hoặc bạn bè.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/loaihoa/summertime.jpeg'
  )

product55 = models.Product(
  name='Cupid Kisses', 
  price=599000,
  description='Các loại hoa tông màu đỏ rực rỡ cắm hình trái tim kết hợp với hộp hoa gỗ vintage cổ điển, là món quà sang trọng thích hợp để tặng nhân dịp khai trương hoặc cho bất kỳ dịp đặc biệt. ', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/loaihoa/cupidkisses.jpeg'
  )

product56 = models.Product(
  name='Intimate Amber', 
  price=869000,
  description='Những loại hoa màu đỏ kết hợp với giỏ hoa kim loại là món quà cổ điển, sang trọng cho bất kỳ ai bạn yêu mến. ', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/loaihoa/intimateamber.jpeg'
  )

product61 = models.Product(
  name='Blissful', 
  price=2559000,
  description='Đây sẽ là món quà tặng đầy ý nghĩa thay cho lời chúc mừng trong dịp khai trương hoặc các ngày lễ trọng đại.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/loaihoa/Blissful.jpeg'
  )

product62 = models.Product(
  name='Pretty Glory', 
  price=759000,
  description='Hộp hoa gỗ hoành tráng với sự góp mặt của đồng tiền và hướng dương - những loài hoa mang ý nghĩa vô cùng tốt lành để bạn gửi tặng trong bất kỳ dịp quan trọng nào. ', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/loaihoa/Pretty.jpeg'
  )

product63 = models.Product(
  name='Magnificient', 
  price=999000,
  description='Đây sẽ là món quà tặng đầy ý nghĩa thay cho lời chúc mừng trong dịp khai trương hoặc các ngày lễ trọng đại.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/loaihoa/Magnificient.jpeg'
  )

category3.products.append(product52)
category3.products.append(product53)
category3.products.append(product54)
category3.products.append(product55)
category3.products.append(product56)
category3.products.append(product61)
category3.products.append(product62)
category3.products.append(product63)

# Sản phẩm khác
product57 = models.Product(
  name='Trà Hoa Hồng Rose (20gr)', 
  price=159000,
  description='Hoa hồng là sự lựa chọn hoàn hảo cho mọi bữa tiệc trà. Đây là một loại trà nhẹ và có hương vị nhẹ nhàng sẽ đưa bạn đến với hương thơm ngọt ngào của vườn hoa hồng.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/sanphamkhac/trahoahongrose.jpeg'
  )

product58 = models.Product(
  name='Bộ Quà Tặng Thiên Nhiên', 
  price=519000,
  description='Khăn sợi tre : Thân thiện làn da người lớn và trẻ nhỏ, kháng khuẩn /kháng vi nấm, duy trì độ mềm mại ổn định dù giặt tay hay giặt máy, nhanh khô, thoáng, không ám mùi.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/sanphamkhac/quatangthiennhien.jpeg'
  )

product59 = models.Product(
  name='Hộp Chocolate Ferrero', 
  price=149000,
  description='Ferrero Rocher là dòng sản phẩm chocolate cao cấp đến từ Ý với hương vị đặc biệt, kết hợp hạt phỉ thơm béo, lớp bánh xốp giòn tan và chocolate hảo hạn phủ bên ngoài.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/sanphamkhac/socolaferrero.jpeg'
  )

product60 = models.Product(
  name='Sweet Innocence', 
  price=799000,
  description='Nằm trong Bộ sưu tập Chào đón Giáng Sinh, Hộp hoa Sweet Innocence với sự kết hợp của Dâu tây thanh mát, chocolate nồng nàn cùng chút tinh khôi của Baby trắng và Cúc tana cuối đông sẽ là một món quà ngọt ngào cho người bạn thương vào dịp Noel này đấy.', 
  quantity=18, 
  discount=0, 
  image_file='image_flower/sanphamkhac/sweetinnocence.jpeg'
  )

category4.products.append(product57)
category4.products.append(product58)
category4.products.append(product59)
category4.products.append(product60)

promo_code = models.PromoCode(code='DC1', discount=10, maximum_usage=2)
user.promo_codes.append(promo_code)

cart = models.Cart()
cart2 = models.Cart()
cart_item1 = models.CartItem(quantity=2)
cart_item1.product = product
cart_item2 = models.CartItem(quantity=3)
cart_item2.product = product2

cart.items.append(cart_item1)
cart.items.append(cart_item2)

order = models.Order(discount=10, name=user.name, phone_number=user.phone_number, address=user.address)
# user.orders.append(order)

user.carts.append(cart)
user.carts.append(cart2)
user2.carts.append(models.Cart())
user3.carts.append(models.Cart())
user4.carts.append(models.Cart())

transaction = models.Transaction(amount=2625, payment_type='momo')
# user.transactions.append(transaction)
order.transaction = transaction

cart.order = order

feedback = models.Feedback(comment='Sản phẩm tốt')
user.feedbacks.append(feedback)
product.feedbacks.append(feedback)

db.session.add(user)
db.session.add(user2)
db.session.add(user3)
db.session.add(user4)
db.session.add(category)
db.session.add(category2)
db.session.add(category3)
db.session.add(category4)
db.session.commit()
