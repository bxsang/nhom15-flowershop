from operator import mod
from config import app, api, jwt
import models
from flask import request
from flask_restplus import Resource, fields
from flask_jwt_extended import current_user, jwt_required

@api.route('/ping')
class Ping(Resource):
    def get(self):
        return 'pong'

@api.route('/login')
class UserLogin(Resource):
    @api.expect(api.model('Login model',
        {
            'email': fields.String(required = True),
            'password': fields.String(required = True)
        }
    ))

    def post(self):
        data = request.get_json()
        user = models.User().get_by_email(data['email'])
        if user is not None:
            if user.check_password(data['password']):
                return {'success': True, 'msg': 'Đăng nhập thành công', 'access_token':  user.generate_jwt()}
            else:
                api.abort(403, 'Sai mật khẩu', success=False)
        else:
                api.abort(403, 'Sai email', success=False)

@api.route('/register')
class UserRegister(Resource):
    @api.expect(api.model('Register model',
        {
            'name': fields.String(required = True),
            'email': fields.String(required = True),
            'password': fields.String(required = True),
            'phone_number': fields.String(required = True),
            'address': fields.String(required = True)
        }
    ))

    def post(self):
        data = request.get_json()
        user = models.User(name=data['name'], email=data['email'], password=data['password'], phone_number=data['phone_number'], address=data['address'])
        return user.create_new()

@api.route('/user/info')
class User(Resource):
    @jwt_required()
    def get(self):
        if current_user is not None:
            return {
                'success': True,
                'result': {
                    'id': current_user.id,
                    'name': current_user.name,
                    'email': current_user.email,
                    'phone_number': current_user.phone_number,
                    'address': current_user.address,
                    'permissions': current_user.permissions,
                    'registered_at': str(current_user.registered_at),
                    'update_at': str(current_user.update_at)
                }
            }
        return {'success': False}

    @jwt_required()
    def put(self):
        data = request.get_json()
        if current_user is not None:
            return current_user.update(data)
        return {
            'success': False,
            'msg': 'User không tồn tại'
        }

@api.route('/client/all')
class GetClientUsers(Resource):
    @jwt_required()
    def get(self):
        client_users = models.User.query.filter_by(permissions=0).all()
        if client_users is not None:
            result = []
            for user in client_users:
                result.append({
                    'id': user.id,
                    'name': user.name,
                    'email': user.email,
                    'phone_number': user.phone_number,
                    'address': user.address,
                    'permissions': user.permissions,
                    'registered_at': str(user.registered_at),
                    'update_at': str(user.update_at)
                })
            return {
                'success': True,
                'result': result
            }
        return {'success': False, 'msg': 'Không có dữ liệU'}

@api.route('/admin/all')
class GetClientUsers(Resource):
    @jwt_required()
    def get(self):
        client_users = models.User.query.filter_by(permissions=1).all()
        if client_users is not None:
            result = []
            for user in client_users:
                result.append({
                    'id': user.id,
                    'name': user.name,
                    'email': user.email,
                    'phone_number': user.phone_number,
                    'address': user.address,
                    'permissions': user.permissions,
                    'registered_at': str(user.registered_at),
                    'update_at': str(user.update_at)
                })
            return {
                'success': True,
                'result': result
            }
        return {'success': False, 'msg': 'Không có dữ liệU'}

@api.route('/categories')
class Categories(Resource):
    def get(self):
        categories = models.Category.query.all()
        if categories is not None:
            result = []
            for category in categories:
                result.append({
                    'id': category.id,
                    'name': category.name
                })
            return {
                'success': True,
                'result': result
            }
        else:
            return {
                'success': False,
                'msg': 'Không tìm thấy thông tin'
            }

@api.route('/product')
class Product(Resource):
    def get(self):
        product_id = request.args.get('product_id')
        product = models.Product().get_by_id(product_id)

        if product is not None:
            feedbacks = []
            for feedback in product.feedbacks:
                user = models.User().get_by_id(feedback.user_id)
                feedbacks.append({
                    'id': feedback.id,
                    'user_id': feedback.user_id,
                    'user_name': user.name,
                    'product_id': feedback.product_id,
                    'comment': feedback.comment,
                    'created_at': str(feedback.created_at),
                    'update_at': str(feedback.update_at)
                })
            return {
                'success': True,
                'result': {
                    'name': product.name,
                    'price': product.price,
                    'category': product.category.name,
                    'description': product.description,
                    'quantity': product.quantity,
                    'discount': product.discount,
                    'image_file': product.image_file,
                    'added_at': str(product.added_at),
                    'update_at': str(product.update_at),
                    'feedbacks': feedbacks
                }
            }
        return {'success': False}
    
    @jwt_required()
    def post(self):
        data = request.get_json()
        new_product = models.Product(name=data['name'], price=int(data['price']), category_id=int(data['category_id']), description=data['description'], quantity=int(data['quantity']), discount=int(data['discount']), image_file=data['image_file'])
        return new_product.create_new()
    
    @jwt_required()
    def put(self):
        data = request.get_json()
        product = models.Product().get_by_id(data['product_id'])
        if product is not None:
            return product.update(data)
        return {
            'success': False,
            'msg': 'Mã SP không tồn tại'
        }
    
    @jwt_required()
    def delete(self):
        data = request.get_json()
        product = models.Product().get_by_id(data['product_id'])
        if product is not None:
            return product.delete()
        return {
            'success': False,
            'msg': 'Mã SP không tồn tại'
        }

@api.route('/product/feedback')
class Feedback(Resource):
    @jwt_required()
    def post(self):
        data = request.get_json()
        product = models.Product().get_by_id(data['product_id'])
        return product.add_feedback(current_user.id, data['comment'])

@api.route('/products/<string:sort_type>')
class GetProductList(Resource):
    def get(self, sort_type):
        size = int(request.args.get('size'))
        offset = int(request.args.get('offset'))
        limit = offset + size
        result = []

        if sort_type not in ['random', 'newest', 'oldest', 'most-ordered', 'alphabetical', 'search']:
            return {'success': False, 'msg': 'Invalid URI'}

        try:
            if sort_type == 'random':
                products = models.Product().get_random(offset, limit)
            elif sort_type == 'newest':
                products = models.Product().get_newest(offset, limit)
            elif sort_type == 'oldest':
                products = models.Product().get_oldest(offset, limit)
            elif sort_type == 'alphabetical':
                products = models.Product().get_alphabetical(offset, limit)
            elif sort_type == 'most-ordered':
                products = models.Product().get_most_ordered(offset, limit)
            elif sort_type == 'search':
                q = request.args.get('q')
                products = models.Product().search(q, offset, limit)
            for product in products:
                if not product.is_deleted:
                    result.append({
                        'id': product.id,
                        'name': product.name,
                        'price': product.price,
                        'category_id': product.category_id,
                        'category_name': product.category.name,
                        'description': product.description,
                        'quantity': product.quantity,
                        'discount': product.discount,
                        'image_file': product.image_file,
                        'added_at': str(product.added_at),
                        'update_at': str(product.update_at)
                    })
            return {'success': True, 'result': result}
        except Exception as e:
            print('Get products failed: ' + str(e))
            return {'success': False, 'message': 'Failed to get products'}

@api.route('/products/cat/<int:category_id>/<string:sort_type>')
class GetProductListByCategory(Resource):
    def get(self, category_id, sort_type):
        size = int(request.args.get('size'))
        offset = int(request.args.get('offset'))
        limit = offset + size
        result = []

        if sort_type not in ['random', 'newest', 'oldest', 'most-ordered', 'alphabetical', 'search']:
            return {'success': False, 'msg': 'Invalid URI'}

        try:
            if sort_type == 'random':
                products = models.Product().get_random(offset, limit)
            elif sort_type == 'newest':
                products = models.Product().get_newest(offset, limit)
            elif sort_type == 'oldest':
                products = models.Product().get_oldest(offset, limit)
            elif sort_type == 'alphabetical':
                products = models.Product().get_alphabetical(offset, limit)
            elif sort_type == 'most-ordered':
                products = models.Product().get_most_ordered(offset, limit)
            elif sort_type == 'search':
                q = request.args.get('q')
                products = models.Product().search(q, offset, limit)
            for product in products:
                if not product.is_deleted and product.category_id==category_id:
                    result.append({
                        'id': product.id,
                        'name': product.name,
                        'price': product.price,
                        'category_id': product.category_id,
                        'category_name': product.category.name,
                        'description': product.description,
                        'quantity': product.quantity,
                        'discount': product.discount,
                        'image_file': product.image_file,
                        'added_at': str(product.added_at),
                        'update_at': str(product.update_at)
                    })
            return {'success': True, 'result': result}
        except Exception as e:
            print('Get products failed: ' + str(e))
            return {'success': False, 'message': 'Failed to get products'}

@api.route('/cart/<int:cart_id>')
class GetCart(Resource):
    def get(self, cart_id):
        cart = models.Cart().get_by_id(cart_id)
        if cart is not None:
            items = []
            for item in cart.items:
                product = models.Product().get_by_id(item.product_id)
                items.append({
                    'product_id': item.product_id,
                    'product_name': product.name,
                    'product_price': product.price,
                    'category': product.category.name,
                    'description': product.description,
                    'quantity': product.quantity,
                    'discount': product.discount,
                    'image_file': product.image_file,
                    'added_at': str(product.added_at),
                    'update_at': str(product.update_at),
                    'quantity': item.quantity
                })
            return {
                'success': True,
                'result': {
                    'user_id': cart.user_id,
                    'created_at': str(cart.created_at),
                    'update_at': str(cart.update_at),
                    'items': items
                }
            }
        return {'success': False}

@api.route('/user/cart')
class GetUserCart(Resource):
    @jwt_required()
    def get(self):
        cart = models.Cart().get_one_by_user_id(current_user.id)
        if cart is not None:
            items = []
            grand_total = 0
            for item in cart.items:
                product = models.Product().get_by_id(item.product_id)
                items.append({
                    'product_id': item.product_id,
                    'product_name': product.name,
                    'product_price': product.price,
                    'category': product.category.name,
                    'description': product.description,
                    'product_quantity': product.quantity,
                    'discount': product.discount,
                    'image_file': product.image_file,
                    'added_at': str(product.added_at),
                    'update_at': str(product.update_at),
                    'cart_quantity': item.quantity
                })
                grand_total += int(product.price)*int(item.quantity)
            return {
                'success': True,
                'result': {
                    'user_id': cart.user_id,
                    'created_at': str(cart.created_at),
                    'update_at': str(cart.update_at),
                    'items': items,
                    'grand_total': grand_total
                }
            }
        return {'success': False}

@api.route('/user/cart/item')
class CartItems(Resource):
    @jwt_required()
    def post(self):
        data = request.get_json()
        cart = models.Cart().get_one_by_user_id(current_user.id)
        return cart.add_product(data['product_id'], data['quantity'])

    @jwt_required()
    def put(self):  # update quantity
        data = request.get_json()
        cart = models.Cart().get_one_by_user_id(current_user.id)
        return cart.update_item_quantity(data['product_id'], data['quantity'])

    @jwt_required()
    def delete(self):
        data = request.get_json()
        cart = models.Cart().get_one_by_user_id(current_user.id)
        return cart.delete_one_product(data['product_id'])

@api.route('/user/order')
class Order(Resource):
    @jwt_required()
    def get(self):
        size = request.args.get('size')
        offset = request.args.get('offset')
        limit = offset + size

        try:
            orders = models.Order().get_by_user_id(current_user.id, offset, limit)
            return {'success': True, 'result': orders}
        except Exception as e:
            print('Get orders failed: ' + str(e))
            return {'success': False, 'message': 'Failed to get orders'}

    @jwt_required()
    def post(self):
        data = request.get_json()
        cart = models.Cart().get_one_by_user_id(current_user.id)
        return cart.orderCart(current_user, data['name'], data['phone_number'], data['address'])

@api.route('/user/all/order')
class AllUserOrder(Resource):
    @jwt_required()
    def get(self):
        size = request.args.get('size')
        offset = request.args.get('offset')
        limit = offset + size

        try:
            orders = models.Order().get_by_all_users(offset, limit)
            return {'success': True, 'result': orders}
        except Exception as e:
            print('Get orders failed: ' + str(e))
            return {'success': False, 'message': 'Failed to get orders'}

@api.route('/admin/overview')
class Overview(Resource):
    @jwt_required()
    def get(self):
        total_sale = 0
        total_order = 0
        total_client = 0
        total_employee = 0
        try:
            orders = models.Order.query.all()
            for order in orders:
                total_order += 1
                for item in order.cart.items:
                    product = models.Product().get_by_id(item.product_id)
                    total_sale += int(product.price)*int(item.quantity)
            users = models.User.query.all()
            for user in users:
                if user.permissions == 0:
                    total_client += 1
                else:
                    total_employee += 1
            return {
                'success': True,
                'result': {
                    'total_sale': total_sale,
                    'total_order': total_order,
                    'total_client': total_client,
                    'total_employee': total_employee
                }
            }
        except Exception as e:
            print(e)
            return {
                'success': False,
                'msg': 'Có lỗi xảy ra, vui lòng thử lại sau!'
            }

@jwt.user_identity_loader
def user_identity_lookup(user):
    return user.email

@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    identity = jwt_data["sub"]
    return models.User().get_by_email(identity)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)
