# flowershop

## Project setup

### Install dependencies
```
npm install
```

### Do not modify the .env file, create your local one instead
- Create a new file named `.env.local`
- Copy the contents of `.env` to `.env.local` and edit the variables inside that new file

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
