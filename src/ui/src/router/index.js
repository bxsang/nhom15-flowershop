import Vue from 'vue'
import VueRouter from 'vue-router'

import ProductHome from '@/components/Product/Home.vue'
import ProductDetail from '@/components/Product/Detail.vue'
import SearchProduct from '@/components/Product/Search.vue'
import AccountInfo from '@/components/Account/Info.vue'
import Cart from '@/components/Product/Cart.vue'
import Order from '@/components/Product/Order.vue'
import Categories from '@/components/Product/Categories.vue'

// Admin
import ListProduct from '@/components/Admin/ListProduct.vue'
import AddProduct from '@/components/Admin/AddProduct.vue'
import ListOrder from '@/components/Admin/ListOrder.vue'
import Customers from '@/components/Admin/Customers.vue'
import Employees from '@/components/Admin/Employee.vue'
import AdminDashboard from '@/components/Admin/Dashboard.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'ProductHome',
    component: ProductHome
  },
  {
    path: '/product/:product_id',
    name: 'ProductDetail',
    component: ProductDetail
  },
  {
    path: '/search/:query',
    name: 'SearchProduct',
    component: SearchProduct
  },
  {
    path: '/account',
    name: 'AccountInfo',
    component: AccountInfo
  },
  {
    path: '/cart',
    name: 'Cart',
    component: Cart
  },
  {
    path: '/orders',
    name: 'Order',
    component: Order
  },
  {
    path: '/categories/:category_id',
    name: 'Categories',
    component: Categories
  },
  // Admin
  {
    path: '/admin/products',
    name: 'ListProduct',
    component: ListProduct
  },
  {
    path: '/admin/add-product',
    name: 'AddProduct',
    component: AddProduct
  },
  {
    path: '/admin/orders',
    name: 'ListOrder',
    component: ListOrder
  },
  {
    path: '/admin/customers',
    name: 'Customers',
    component: Customers
  },
  {
    path: '/admin/employees',
    name: 'Employees',
    component: Employees
  },
  {
    path: '/admin',
    name: 'AdminDashboard',
    component: AdminDashboard
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
