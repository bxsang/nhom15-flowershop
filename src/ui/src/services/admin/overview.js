import Repository from '../repository'

class AdminOverviewService {
  async getOverview () {
    const response = await Repository.get('/admin/overview')

    if (!response.success) {
      throw new Error(response.message)
    }
    return response.result
  }
}

export default new AdminOverviewService()
