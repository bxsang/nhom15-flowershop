import Repository from './repository'

class UserService {
  async login (credentials) {
    const endpoint = '/login'
    const response = await Repository.post(endpoint, credentials)
    if (response.success && response.access_token != null) {
      localStorage.setItem('user', JSON.stringify(
        { email: credentials.email, access_token: response.access_token }
      ))
    } else {
      throw new Error('Login failed')
    }
    return response
  }

  async register (info) {
    const endpoint = '/register'
    const response = await Repository.post(endpoint, info)
    if (response.success) {
      return true
    } else {
      throw new Error('Login failed')
    }
  }

  logout () {
    localStorage.removeItem('user')
  }

  async getInfo () {
    const response = await Repository.get('/user/info')

    if (!response.success) {
      throw new Error(response.msg)
    }
    return response
  }

  async updateInfo (info, password) {
    const data = {
      name: info.name,
      current_password: password.currentPassword,
      new_password: password.newPassword,
      phone_number: info.phone_number,
      address: info.address
    }
    const response = await Repository.put('/user/info', data)
    return response
  }

  async getCustomers () {
    const response = await Repository.get('/client/all')

    if (!response.success) {
      throw new Error(response.msg)
    }
    return response.result
  }

  async getEmployees () {
    const response = await Repository.get('/admin/all')

    if (!response.success) {
      throw new Error(response.msg)
    }
    return response.result
  }
}

export default new UserService()
