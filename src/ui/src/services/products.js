import Repository from './repository'

class ProductsService {
  async getProductList (type, size, offset) {
    const response = await Repository.get(`/products/${type}?size=${size}&offset=${offset}`)

    if (!response.success) {
      throw new Error(response.msg)
    }
    return response.result
  }

  async getProductListByCategory (categoryId, type, size, offset) {
    const response = await Repository.get(`/products/cat/${categoryId}/${type}?size=${size}&offset=${offset}`)

    if (!response.success) {
      throw new Error(response.msg)
    }
    return response.result
  }

  async searchProduct (query, size, offset) {
    const response = await Repository.get(`/products/search?q=${query}&size=${size}&offset=${offset}`)

    if (!response.success) {
      throw new Error(response.msg)
    }
    return response.result
  }

  async getProductDetail (productId) {
    const response = await Repository.get(`/product?product_id=${productId}`)

    if (!response.success) {
      throw new Error(response.msg)
    }
    return response.result
  }

  async addFeedback (productId, comment) {
    const data = {
      product_id: productId,
      comment: comment
    }
    const response = await Repository.post('/product/feedback', data)
    return response
  }

  async getCategories () {
    const response = await Repository.get('/categories')

    if (!response.success) {
      throw new Error(response.msg)
    }
    return response.result
  }

  async addProduct (product) {
    const data = {
      name: product.name,
      price: product.price,
      category_id: product.category_id,
      description: product.description,
      quantity: product.quantity,
      discount: product.discount,
      image_file: product.image_file
    }
    const response = await Repository.post('/product', data)
    return response
  }

  async deleteProduct (productId) {
    const data = { product_id: productId }
    const response = await Repository.delete('/product', data)

    if (!response.success) {
      throw new Error(response.msg)
    }
    return response
  }
}

export default new ProductsService()
