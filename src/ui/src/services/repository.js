const API_URL = process.env.VUE_APP_API_URL

function getToken () {
  try {
    return JSON.parse(localStorage.getItem('user')).access_token
  } catch (error) {
    return ''
  }
}

class Repository {
  token = ''

  async get (endpoint) {
    this.token = getToken()
    const response = await fetch(`${API_URL}${endpoint}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${this.token}`
      }
    })
    return response.json()
  }

  async post (endpoint, data) {
    this.token = getToken()
    const response = await fetch(`${API_URL}${endpoint}`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${this.token}`
      },
      body: JSON.stringify(data)
    })
    return response.json()
  }

  async put (endpoint, data) {
    this.token = getToken()
    const response = await fetch(`${API_URL}${endpoint}`, {
      method: 'PUT',
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${this.token}`
      },
      body: JSON.stringify(data)
    })
    return response.json()
  }

  async delete (endpoint, data) {
    this.token = getToken()
    const response = await fetch(`${API_URL}${endpoint}`, {
      method: 'DELETE',
      headers: {
        'Content-type': 'application/json',
        Authorization: `Bearer ${this.token}`
      },
      body: JSON.stringify(data)
    })
    return response.json()
  }
}

export default new Repository()
