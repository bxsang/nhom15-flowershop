import Repository from './repository'

class CartService {
  async getCart () {
    const response = await Repository.get('/user/cart')

    if (!response.success) {
      throw new Error(response.message)
    }
    return response.result
  }

  async addProduct (productId, quantity) {
    const data = {
      product_id: productId,
      quantity: quantity
    }
    const response = await Repository.post('/user/cart/item', data)
    return response
  }

  async updateCartProductQuantity (productId, quantity) {
    const data = {
      product_id: productId,
      quantity: quantity
    }
    const response = await Repository.put('/user/cart/item', data)
    return response
  }

  async deleteProductFromOrder (productId) {
    const data = {
      product_id: productId
    }
    const response = await Repository.delete('/user/cart/item', data)
    return response
  }

  async order (name, phoneNumber, address) {
    const data = {
      name: name,
      phone_number: phoneNumber,
      address: address
    }
    const response = await Repository.post('/user/order', data)
    return response
  }

  async getOrders (size, offset) {
    const response = await Repository.get(`/user/order?size=${size}&offset=${offset}`)

    if (!response.success) {
      throw new Error(response.message)
    }
    return response.result
  }

  async getUsersOrders (size, offset) {
    const response = await Repository.get(`/user/all/order?size=${size}&offset=${offset}`)

    if (!response.success) {
      throw new Error(response.message)
    }
    return response.result
  }
}

export default new CartService()
