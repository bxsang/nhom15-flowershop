import UserService from '@/services/user'

const user = JSON.parse(localStorage.getItem('user'))
const initialState = user
  ? { loggedIn: true, email: user.email }
  : { loggedIn: false, email: null }
const initialState2 = { registerSuccess: false }
const initialState3 = { info: null }

export default {
  namespaced: true,
  state: { ...initialState, ...initialState2, ...initialState3 },
  actions: {
    login ({ commit }, credentials) {
      return UserService.login(credentials).then(
        (response) => {
          commit('loginSuccess', credentials.email)
          return Promise.resolve(response)
        },
        (error) => {
          commit('registerFail')
          return Promise.reject(error)
        }
      )
    },
    register ({ commit }, info) {
      return UserService.register(info).then(
        (success) => {
          commit('registerSuccess')
          return Promise.resolve(success)
        },
        (error) => {
          commit('registerSuccess')
          return Promise.reject(error)
        }
      )
    },
    logout ({ commit }) {
      UserService.logout()
      commit('logoutSuccess')
    },
    getInfo ({ commit }) {
      return UserService.getInfo().then(
        (response) => {
          commit('getInfoSuccess', response.result)
          return Promise.resolve(response)
        }
      )
    }
  },
  mutations: {
    loginSuccess (state, email) {
      state.loggedIn = true
      state.email = email
    },
    loginFailure (state) {
      state.loggedIn = false
      state.email = null
    },
    logoutSuccess (state) {
      state.loggedIn = false
      state.email = null
    },
    registerSuccess (state) {
      state.registerSuccess = true
    },
    registerFail (state) {
      state.registerSuccess = false
    },
    getInfoSuccess (state, info) {
      state.info = info
    }
  }
}
