export default {
  namespaced: true,
  state: {
    loginModalShow: false,
    successModalShow: false,
    failedModalShow: false
  },
  actions: {
    toggleLoginModal ({ commit }) {
      commit('toggleLoginModal')
    },
    showLoginModal ({ commit }) {
      commit('showLoginModal')
    },
    hideLoginModal ({ commit }) {
      commit('hideLoginModal')
    },
    showSuccessModal ({ commit }) {
      commit('showSuccessModal')
    },
    hideSuccessModal ({ commit }) {
      commit('hideSuccessModal')
    },
    showFailedModal ({ commit }) {
      commit('showFailedModal')
    },
    hideFailedModal ({ commit }) {
      commit('hideFailedModal')
    }
  },
  mutations: {
    toggleLoginModal (state) {
      state.loginModalShow = !state.loginModalShow
    },
    showLoginModal (state) {
      state.loginModalShow = true
    },
    hideLoginModal (state) {
      state.loginModalShow = false
    },
    showSuccessModal (state) {
      state.successModalShow = true
    },
    hideSuccessModal (state) {
      state.successModalShow = false
    },
    showFailedModal (state) {
      state.failedModalShow = true
    },
    hideFailedModal (state) {
      state.failedModalShow = false
    }
  }
}
