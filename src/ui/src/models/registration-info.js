export default class RegistrationInfo {
  constructor (name, email, password, phoneNumber, address) {
    this.name = name
    this.email = email
    this.password = password
    this.phoneNumber = phoneNumber
    this.address = address
  }
}
