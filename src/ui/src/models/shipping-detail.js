export default class ShippingDetail {
  constructor (name, phoneNumber, address) {
    this.name = name
    this.phoneNumber = phoneNumber
    this.address = address
  }
}
