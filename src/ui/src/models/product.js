export default class Product {
  constructor (id, name, price, categoryId, description, quantity, discount, imageFile) {
    this.id = id
    this.name = name
    this.price = price
    this.categoryId = categoryId
    this.description = description
    this.quantity = quantity
    this.discount = discount
    this.imageFile = imageFile
  }
}
