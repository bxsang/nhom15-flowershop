# Hướng dẫn chạy code

## 1. Cài đặt các phần mềm cần thiết
- Nodejs (https://nodejs.org/en/download/)
- Python 3 (https://www.python.org/downloads/)

## 2. Clone repo về máy
```sh
git clone https://gitlab.com/bxsang/nhom15-flowershop.git
```

## 3. Import file `init.sql` vào MySQL hoặc MariaDB

## 4. Tạo các file biến môi trường (.env)
### Backend
- Trong thư mục src/api tạo bản sao của file .env.example và đổi tên thành .env
- Chỉnh sửa các thông số như server, user của CSDL nếu cần ở trong file .env
### Frontend
- Trong thư mục src/ui tạo bản sao của file .env và đổi tên thành .env.local
- Chỉnh url của backend trong file .env.local

## 5. Chạy server api
- Vào thư mục src/api
- Chạy `python -m venv env` để tạo môi trường ảo thực thi code (chỉ cần thực hiện 1 lần đầu)
- Chạy `env\Scripts\activate` (đối với Windows) hoặc `source env/bin/activate`(macOS, Linux) để kích hoạt môi trường env
- Chạy `pip install -r requirements.txt` để cài đặt các thư viện cần thiết
- Thực thi chương trình: `python app.py`

## 6. Chạy server frontend
- Vào thư mục src/ui
- Chạy `npm install` để cài đặt các thư viện cần thiết
- Thực thi chương trình: `npm run serve`
